# [1.0.0-next.2](https://gitlab.com/monster-space-network/typemon/aamon/platform-lambda/compare/1.0.0-next.1...1.0.0-next.2) (2021-02-11)


### Bug Fixes

* **http.runtime:** 잘못된 빈 값 직렬화 문제 해결 ([84a53c8](https://gitlab.com/monster-space-network/typemon/aamon/platform-lambda/commit/84a53c8a400d5d9bb9a34b3a56f2c013dcd7751d))



# [1.0.0-next.1](https://gitlab.com/monster-space-network/typemon/aamon/platform-lambda/compare/1.0.0-next.0...1.0.0-next.1) (2021-01-31)


### Features

* **http.request:** base64 속성 추가 ([cb12511](https://gitlab.com/monster-space-network/typemon/aamon/platform-lambda/commit/cb1251196324f99a3b4c1b3e78040abbd23505fb))
* **http.request:** method 속성 추가 및 path 속성 값 변경 ([14ef65c](https://gitlab.com/monster-space-network/typemon/aamon/platform-lambda/commit/14ef65cda8542f7c06e82999e60c5b341b5a08e5))
* **http.request:** path 속성 추가 ([4d60890](https://gitlab.com/monster-space-network/typemon/aamon/platform-lambda/commit/4d60890051e4e5c256692dcc9a0cf7e21429bb66))
* **http.response:** base64 인코딩 여부 속성 이름 변경 ([6c9398c](https://gitlab.com/monster-space-network/typemon/aamon/platform-lambda/commit/6c9398cbd2d41b3a870c55bdcf8e0442953d9e97))
* **http.response:** isBase64Encoded 속성 추가 ([4d89fd1](https://gitlab.com/monster-space-network/typemon/aamon/platform-lambda/commit/4d89fd1317592511c0cf0a4860cdd9cf2c8be482))
* @typemon/pipeline 의존성 추가 ([626316e](https://gitlab.com/monster-space-network/typemon/aamon/platform-lambda/commit/626316eb8c3d1e95171c975d54cb3e5fd26166ae))
* 의존성 업데이트 ([e6e8cc8](https://gitlab.com/monster-space-network/typemon/aamon/platform-lambda/commit/e6e8cc892b0fb8d9a53c312dd95b5e325559f5f9))
* 파라미터 식별자 변경 ([c98655f](https://gitlab.com/monster-space-network/typemon/aamon/platform-lambda/commit/c98655f61555b8128a4611cf17d8b2d0487aee89))



