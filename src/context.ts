import { Inject } from '@typemon/dependency-injection';
import type Lambda from 'aws-lambda';

export type Context = Lambda.Context;
export function Context(): ParameterDecorator {
    return Inject('aamon.platform-lambda.parameters.context');
}
