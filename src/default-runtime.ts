import { Parameters, Runtime } from '@aamon/core';

import { Event } from './event';
import { Context } from './context';

const RESPONSE: unique symbol = Symbol.for('aamon.platform-lambda.default-runtime.response');

export class DefaultRuntime implements Runtime {
    private [RESPONSE]: unknown;

    public constructor(
        protected readonly event: Event,
        protected readonly context: Context,
    ) {
        this[RESPONSE] = null;
    }

    public initialize(parameters: Parameters): void {
        parameters.set('aamon.platform-lambda.parameters.event', this.event);
        parameters.set('aamon.platform-lambda.parameters.context', this.context);
    }

    public output(output: unknown): void {
        this[RESPONSE] = output ?? this[RESPONSE];
    }

    public getResult(): unknown {
        return this[RESPONSE];
    }
}
