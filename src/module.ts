import { Handler } from '@aamon/core';
import { Constructor, Provider } from '@typemon/dependency-injection';

export interface Module {
    /**
     * @default true
     */
    readonly http?: boolean;

    readonly handler: Handler.Constructor;
    readonly providers?: ReadonlyArray<Constructor | Provider>;
}
