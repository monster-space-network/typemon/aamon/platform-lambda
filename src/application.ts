import { Aamon, Exception } from '@aamon/core';
import { Check } from '@typemon/check';

import { Module } from './module';
import { DefaultRuntime } from './default-runtime';
import { Event } from './event';
import { Context } from './context';
import { HttpRuntime } from './http';

export type Application = (event: Event, context: Context) => Promise<unknown>;
export namespace Application {
    export function create(module: Module): Application {
        const runtimeConstructor: new (event: Event, context: Context) => DefaultRuntime = Check.isNotFalse(module.http)
            ? HttpRuntime
            : DefaultRuntime;
        const aamonPromise: Promise<Aamon> = Aamon.create({
            handlers: [module.handler],
            providers: module.providers,
        });

        return async (event: Event, context: Context): Promise<unknown> => {
            const aamon: Aamon = await aamonPromise;
            const runtime: DefaultRuntime = new runtimeConstructor(event, context);
            const exception: null | Exception = await aamon.execute(runtime, module.handler);

            if (Check.isNotNull(exception)) {
                throw exception.error;
            }

            return runtime.getResult();
        };
    }
}
