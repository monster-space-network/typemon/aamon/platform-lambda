import { Inject } from '@typemon/dependency-injection';

export type Event = any;
export function Event(): ParameterDecorator {
    return Inject('aamon.platform-lambda.parameters.event');
}
