import { Check } from '@typemon/check';
import { Runtime, ParameterMetadata, Parameters, Exception } from '@aamon/core';
import type { APIGatewayProxyEventV2, APIGatewayProxyResultV2 } from 'aws-lambda';
import Boom from '@hapi/boom';

import { MetadataKey } from '../metadata-key';
import { DefaultRuntime } from '../default-runtime';
import { Context } from '../context';
import { HttpRequest } from './http-request';
import { HttpResponse } from './http-response';
import { ReadonlyHttpParameters } from './http-parameters';

function resolveNamedParameter(parameters: ReadonlyHttpParameters, metadata: ParameterMetadata): undefined | string | ReadonlyHttpParameters {
    if (metadata.hasNot(MetadataKey.NAME)) {
        return parameters;
    }

    const name: string = metadata.get(MetadataKey.NAME);

    if (parameters.hasNot(name) && metadata.optional) {
        return;
    }

    return parameters.get(name);
}

export class HttpRuntime extends DefaultRuntime implements Runtime {
    private readonly request: HttpRequest;
    private readonly response: HttpResponse;

    public constructor(event: APIGatewayProxyEventV2, context: Context) {
        super(event, context);

        this.request = new HttpRequest(this.event);
        this.response = new HttpResponse();
    }

    public initialize(parameters: Parameters): void {
        super.initialize(parameters);
        parameters.set('aamon.platform-lambda.http.parameters.request', this.request);
        parameters.set('aamon.platform-lambda.http.parameters.request.headers', this.request.headers, resolveNamedParameter);
        parameters.set('aamon.platform-lambda.http.parameters.request.path-parameters', this.request.pathParameters, resolveNamedParameter);
        parameters.set('aamon.platform-lambda.http.parameters.request.query-string-parameters', this.request.queryStringParameters, resolveNamedParameter);
        parameters.set('aamon.platform-lambda.http.parameters.request.body', this.request.body);
        parameters.set('aamon.platform-lambda.http.parameters.response', this.response);
    }

    public output(output: unknown): void {
        this.response.body = output ?? this.response.body;
    }

    public exception(exception: Exception): void {
        const boom: Boom.Boom = Boom.isBoom(exception.error)
            ? exception.error
            : Boom.internal();

        this.response.statusCode = boom.output.statusCode;
        this.response.body = {
            ...boom.output.payload,
            data: boom.data ?? null,
        };
    }

    public getResult(): APIGatewayProxyResultV2 {
        return {
            statusCode: this.response.statusCode,
            headers: Object.fromEntries(this.response.headers),
            body: Check.isUndefinedOrNull(this.response.body)
                ? ''
                : Check.isNotString(this.response.body)
                    ? JSON.stringify(this.response.body)
                    : this.response.body,
            isBase64Encoded: this.response.base64,
        };
    }
}
