import { Inject } from '@typemon/dependency-injection';

import { HttpParameters } from './http-parameters';

export class HttpResponse {
    /**
     * @default 200
     */
    public statusCode: number;

    public readonly headers: HttpParameters;

    /**
     * @default null
     */
    public body: any;

    /**
     * @default false
     */
    public base64: boolean;

    public constructor() {
        this.statusCode = 200;

        this.headers = new HttpParameters('Header');

        this.body = null;

        this.base64 = false;
    }
}

export type Response = HttpResponse;
export function Response(): ParameterDecorator {
    return Inject('aamon.platform-lambda.http.parameters.response');
}
