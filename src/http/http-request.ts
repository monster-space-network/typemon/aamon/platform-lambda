import { Metadata } from '@typemon/metadata';
import { Inject } from '@typemon/dependency-injection';
import type { APIGatewayProxyEventV2 } from 'aws-lambda';

import { MetadataKey } from '../metadata-key';
import { ReadonlyHttpParameters, HttpParameters } from './http-parameters';

export class HttpRequest {
    public readonly method: string;
    public readonly path: string;

    public readonly headers: ReadonlyHttpParameters;
    public readonly pathParameters: ReadonlyHttpParameters;
    public readonly queryStringParameters: ReadonlyHttpParameters;

    public readonly base64: boolean;

    public readonly body: any;

    public constructor(event: APIGatewayProxyEventV2) {
        this.method = event.requestContext.http.method;
        this.path = event.requestContext.http.path;

        this.headers = new HttpParameters('Header', event.headers);
        this.pathParameters = new HttpParameters('Path parameter', event.pathParameters ?? {});
        this.queryStringParameters = new HttpParameters('Query string parameter', event.queryStringParameters ?? {});

        this.base64 = event.isBase64Encoded;

        this.body = event.body ?? '';
    }
}

export type Request = HttpRequest;
export function Request(): ParameterDecorator {
    return Inject('aamon.platform-lambda.http.parameters.request');
}

export type Headers = ReadonlyHttpParameters;
export function Headers(): ParameterDecorator {
    return Inject('aamon.platform-lambda.http.parameters.request.headers');
}

export type Header = string;
export function Header(name: string): ParameterDecorator {
    return Metadata.decorator((metadata: Metadata): void => {
        metadata.decorate([
            Headers(),
        ]);
        metadata.set(MetadataKey.NAME, name);
    });
}

export type PathParameters = ReadonlyHttpParameters;
export function PathParameters(): ParameterDecorator {
    return Inject('aamon.platform-lambda.http.parameters.request.path-parameters');
}

export type PathParameter = string;
export function PathParameter(name: string): ParameterDecorator {
    return Metadata.decorator((metadata: Metadata): void => {
        metadata.decorate([
            PathParameters(),
        ]);
        metadata.set(MetadataKey.NAME, name);
    });
}

export type QueryStringParameters = ReadonlyHttpParameters;
export function QueryStringParameters(): ParameterDecorator {
    return Inject('aamon.platform-lambda.http.parameters.request.query-string-parameters');
}

export type QueryStringParameter = string;
export function QueryStringParameter(name: string): ParameterDecorator {
    return Metadata.decorator((metadata: Metadata): void => {
        metadata.decorate([
            QueryStringParameters(),
        ]);
        metadata.set(MetadataKey.NAME, name);
    });
}

export type Body = any;
export function Body(): ParameterDecorator {
    return Inject('aamon.platform-lambda.http.parameters.request.body');
}
