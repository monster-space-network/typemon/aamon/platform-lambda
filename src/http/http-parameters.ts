import { Check } from '@typemon/check';

export interface ReadonlyHttpParameters extends Iterable<[string, string]> {
    has(name: string): boolean;
    hasNot(name: string): boolean;

    get(name: string): string;
}

export class HttpParameters implements ReadonlyHttpParameters {
    private readonly data: Map<string, string>;

    public constructor(
        private readonly type: string,
        data: object = {},
    ) {
        this.data = new Map(Object.entries(data));
    }

    public [Symbol.iterator](): Iterator<[string, string]> {
        return this.data[Symbol.iterator]();
    }

    public has(name: string): boolean {
        return this.data.has(name);
    }
    public hasNot(name: string): boolean {
        return Check.isFalse(this.has(name));
    }

    public get(name: string): string {
        if (this.hasNot(name)) {
            throw new Error(`${this.type} '${name}' does not exist.`);
        }

        return this.data.get(name) as string;
    }

    public set(name: string, value: string): void {
        this.data.set(name, value);
    }

    public delete(name: string): void {
        this.data.delete(name);
    }
}
